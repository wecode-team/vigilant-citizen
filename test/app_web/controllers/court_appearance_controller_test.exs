defmodule AppWeb.CourtAppearanceControllerTest do
  use AppWeb.ConnCase

  alias App.Cases

  @create_attrs %{date: "some date", remarks: "some remarks"}
  @update_attrs %{date: "some updated date", remarks: "some updated remarks"}
  @invalid_attrs %{date: nil, remarks: nil}

  def fixture(:court_appearance) do
    {:ok, court_appearance} = Cases.create_court_appearance(@create_attrs)
    court_appearance
  end

  describe "index" do
    test "lists all court_appearances", %{conn: conn} do
      conn = get conn, court_appearance_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Court appearances"
    end
  end

  describe "new court_appearance" do
    test "renders form", %{conn: conn} do
      conn = get conn, court_appearance_path(conn, :new)
      assert html_response(conn, 200) =~ "New Court appearance"
    end
  end

  describe "create court_appearance" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, court_appearance_path(conn, :create), court_appearance: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == court_appearance_path(conn, :show, id)

      conn = get conn, court_appearance_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Court appearance"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, court_appearance_path(conn, :create), court_appearance: @invalid_attrs
      assert html_response(conn, 200) =~ "New Court appearance"
    end
  end

  describe "edit court_appearance" do
    setup [:create_court_appearance]

    test "renders form for editing chosen court_appearance", %{conn: conn, court_appearance: court_appearance} do
      conn = get conn, court_appearance_path(conn, :edit, court_appearance)
      assert html_response(conn, 200) =~ "Edit Court appearance"
    end
  end

  describe "update court_appearance" do
    setup [:create_court_appearance]

    test "redirects when data is valid", %{conn: conn, court_appearance: court_appearance} do
      conn = put conn, court_appearance_path(conn, :update, court_appearance), court_appearance: @update_attrs
      assert redirected_to(conn) == court_appearance_path(conn, :show, court_appearance)

      conn = get conn, court_appearance_path(conn, :show, court_appearance)
      assert html_response(conn, 200) =~ "some updated date"
    end

    test "renders errors when data is invalid", %{conn: conn, court_appearance: court_appearance} do
      conn = put conn, court_appearance_path(conn, :update, court_appearance), court_appearance: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Court appearance"
    end
  end

  describe "delete court_appearance" do
    setup [:create_court_appearance]

    test "deletes chosen court_appearance", %{conn: conn, court_appearance: court_appearance} do
      conn = delete conn, court_appearance_path(conn, :delete, court_appearance)
      assert redirected_to(conn) == court_appearance_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, court_appearance_path(conn, :show, court_appearance)
      end
    end
  end

  defp create_court_appearance(_) do
    court_appearance = fixture(:court_appearance)
    {:ok, court_appearance: court_appearance}
  end
end
