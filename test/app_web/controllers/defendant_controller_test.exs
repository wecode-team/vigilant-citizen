defmodule AppWeb.DefendantControllerTest do
  use AppWeb.ConnCase

  alias App.Cases

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:defendant) do
    {:ok, defendant} = Cases.create_defendant(@create_attrs)
    defendant
  end

  describe "index" do
    test "lists all defendants", %{conn: conn} do
      conn = get conn, defendant_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Defendants"
    end
  end

  describe "new defendant" do
    test "renders form", %{conn: conn} do
      conn = get conn, defendant_path(conn, :new)
      assert html_response(conn, 200) =~ "New Defendant"
    end
  end

  describe "create defendant" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, defendant_path(conn, :create), defendant: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == defendant_path(conn, :show, id)

      conn = get conn, defendant_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Defendant"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, defendant_path(conn, :create), defendant: @invalid_attrs
      assert html_response(conn, 200) =~ "New Defendant"
    end
  end

  describe "edit defendant" do
    setup [:create_defendant]

    test "renders form for editing chosen defendant", %{conn: conn, defendant: defendant} do
      conn = get conn, defendant_path(conn, :edit, defendant)
      assert html_response(conn, 200) =~ "Edit Defendant"
    end
  end

  describe "update defendant" do
    setup [:create_defendant]

    test "redirects when data is valid", %{conn: conn, defendant: defendant} do
      conn = put conn, defendant_path(conn, :update, defendant), defendant: @update_attrs
      assert redirected_to(conn) == defendant_path(conn, :show, defendant)

      conn = get conn, defendant_path(conn, :show, defendant)
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, defendant: defendant} do
      conn = put conn, defendant_path(conn, :update, defendant), defendant: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Defendant"
    end
  end

  describe "delete defendant" do
    setup [:create_defendant]

    test "deletes chosen defendant", %{conn: conn, defendant: defendant} do
      conn = delete conn, defendant_path(conn, :delete, defendant)
      assert redirected_to(conn) == defendant_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, defendant_path(conn, :show, defendant)
      end
    end
  end

  defp create_defendant(_) do
    defendant = fixture(:defendant)
    {:ok, defendant: defendant}
  end
end
