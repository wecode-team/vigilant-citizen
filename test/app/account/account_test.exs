defmodule App.AccountTest do
  use App.DataCase

  alias App.Account

  describe "users" do
    alias App.Account.User

    @valid_attrs %{access_token: "some access_token", access_token_secret: "some access_token_secret", handle: "some handle", twitter_id: 42}
    @update_attrs %{access_token: "some updated access_token", access_token_secret: "some updated access_token_secret", handle: "some updated handle", twitter_id: 43}
    @invalid_attrs %{access_token: nil, access_token_secret: nil, handle: nil, twitter_id: nil}

    def user_fixture(attrs \\ %{}) do
      {:ok, user} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Account.create_user()

      user
    end

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Account.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Account.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      assert {:ok, %User{} = user} = Account.create_user(@valid_attrs)
      assert user.access_token == "some access_token"
      assert user.access_token_secret == "some access_token_secret"
      assert user.handle == "some handle"
      assert user.twitter_id == 42
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Account.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      assert {:ok, user} = Account.update_user(user, @update_attrs)
      assert %User{} = user
      assert user.access_token == "some updated access_token"
      assert user.access_token_secret == "some updated access_token_secret"
      assert user.handle == "some updated handle"
      assert user.twitter_id == 43
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Account.update_user(user, @invalid_attrs)
      assert user == Account.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Account.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Account.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Account.change_user(user)
    end
  end
end
