# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :app,
  ecto_repos: [App.Repo]

# config :app, App.Scheduler,
#   jobs: [
#     # Every 3rd minute
#     {"*/3 * * * *", {App.Tweeter, :send, []}},
#   ]

config :extwitter, :oauth, [
  consumer_key: "Ti4g7MKV0xnYwjHE3k7wQjzP2",
  consumer_secret: "oxzkjXBn4YHeRKLUwW9RGIrTwm2Ap4IP7yAiR9EZJIgblwsMjI",
]

# Configures the endpoint
config :app, AppWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "L653Z+0g1GMK1F110mK2Bx6LwVvsosUzMYrj5pSgUZgfRGuiRRE23IGvUcaOuDq5",
  render_errors: [view: AppWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: App.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
