import MainView from './main';
import PageIndexView from './page/index';
import PageLearnView from './page/learn';

// Collection of specific view modules
const views = {
  PageIndexView,
  PageLearnView
};

export default function loadView(viewName) {
  return views[viewName] || MainView;
}

