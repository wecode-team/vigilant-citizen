defmodule AppWeb.PageController do
  use AppWeb, :controller
  alias App.{Accounts, Posts, Cases}

  def index(conn, _params) do
    defendants = Cases.list_defendants()
    events = Cases.list_court_appearances()
    articles = Posts.list_articles()
    render conn, "index.html", defendants: defendants, events: events, articles: articles
  end

  def learn(conn, _params) do
    articles = Posts.list_articles()
    render conn, "learn.html", articles: articles
  end


  def auth(conn, _params) do
    token = ExTwitter.request_token(
      # page_url(conn, :callback)
      "http://54.202.217.143/twitter-callback"
    )
    # IO.inspect(token, label: "token")
    {:ok, authenticate_url} = ExTwitter.authenticate_url(token.oauth_token)
    redirect conn, external: authenticate_url
  end

  def callback(conn, %{"oauth_token" => oauth_token, "oauth_verifier" => oauth_verifier}) do
    {:ok, access_token} = ExTwitter.access_token(oauth_verifier, oauth_token)

    ExTwitter.configure(
      :process,
      Enum.concat(
        ExTwitter.Config.get_tuples,
        [ access_token: access_token.oauth_token,
          access_token_secret: access_token.oauth_token_secret ]
      )
    )

    user_info = ExTwitter.verify_credentials()
    # IO.inspect(user_info, label: "user_info")
    user_deets = %{twitter_id: user_info.id,
      handle: user_info.screen_name,
      access_token: access_token.oauth_token,
      access_token_secret: access_token.oauth_token_secret}
    IO.inspect(user_deets, label: "user deets")

    {:ok, user} = Accounts.create_user(user_deets)
    App.Tweeter.send(user)

    # IO.inspect user_info, label: "user_info"
    conn
    |> put_flash(:info, "Thank you for helping to fight corruption")
    |> redirect(to: page_path(conn, :index))
  end


  def callback(conn, %{"denied" => _}=resp) do
    IO.inspect resp
    conn
    |> put_flash(:error, "You did not give us access")
    |> redirect(to: page_path(conn, :index))
  end
end
