defmodule App.Posts.Article do
  use Ecto.Schema
  import Ecto.Changeset
  alias App.Posts.Article


  schema "articles" do
    field :body, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(%Article{} = article, attrs) do
    article
    |> cast(attrs, [:title, :body])
    |> validate_required([:title, :body])
  end
end
