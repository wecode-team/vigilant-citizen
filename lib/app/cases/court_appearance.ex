defmodule App.Cases.CourtAppearance do
  use Ecto.Schema
  import Ecto.Changeset
  alias App.Cases.{CourtAppearance, Defendant}


  schema "court_appearances" do
    field :date, :string
    field :remarks, :string, default: ""
    belongs_to :defendant, Defendant

    timestamps()
  end

  @doc false
  def changeset(%CourtAppearance{} = court_appearance, attrs) do
    court_appearance
    |> cast(attrs, [:date, :remarks, :defendant_id])
    |> validate_required([:date])
  end
end
