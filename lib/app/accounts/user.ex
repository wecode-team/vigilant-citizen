defmodule App.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias App.Accounts.User


  schema "users" do
    field :access_token, :string
    field :access_token_secret, :string
    field :handle, :string
    field :twitter_id, :integer

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:access_token, :access_token_secret, :handle, :twitter_id])
    |> validate_required([:access_token, :access_token_secret, :handle, :twitter_id])
  end
end
