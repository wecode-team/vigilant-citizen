defmodule App.Tweeter do
  alias App.{Posts, Accounts}

  def send(user) do
    tweets = get_tweets() |> IO.inspect(label: "tweets")

    case Enum.count(tweets) do
      0 -> :ok
      _ ->
        ExTwitter.configure(
          :process,
          Enum.concat(
            ExTwitter.Config.get_tuples,
            [ access_token: user.access_token,
              access_token_secret: user.access_token_secret ]
          )
        )
        Enum.each(tweets, &ExTwitter.update(&1))
        :ok
    end
  end

  def get_tweets do
    article = 
      Posts.list_articles()
      |> Enum.filter(&String.contains?(&1.title, "ACJA"))
      |> Enum.shuffle
      |> Enum.map(fn %{title: title, body: body} ->  
        "#{title}\n#{body}\n#{@hashtags}"
      end)
      |> Enum.random
    # Break up into thread if too long
    tweets =
      article
      |> to_charlist
      |> Enum.chunk_every(275)
      |> Enum.with_index

    case Enum.count(tweets) do
      0 -> []
      1 -> 
        {tw, _} = Enum.at(tweets, 0)
        [tw]
      count ->
        tweets
        |> Enum.map(fn {tw, idx} -> "#{tw} #{idx + 1}/#{count}" end)
    end
  end

  @hashtags "#VigilantCitizens"

  def send do
    users = Accounts.list_users()
    IO.inspect(Enum.count(users), label: "# users")
    IO.puts "users:"
    Enum.each(users, fn user -> IO.puts user.handle end)
    Enum.each(users, &__MODULE__.send(&1))
  end
end
