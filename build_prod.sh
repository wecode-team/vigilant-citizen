#!/bin/bash
mix deps.get
cd assets
npm install
# Rebuild node-sass
npm rebuild node-sass
# After NPM has finished doing its own thing, kindly install the right version of Bootstrap
npm un bootstrap
npm i -S bootstrap@4.0.0-alpha.6
npm un jquery
./node_modules/brunch/bin/brunch b -p
cd ..
MIX_ENV=prod mix phx.digest
MIX_ENV=prod mix release

