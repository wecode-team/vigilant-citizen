defmodule App.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :access_token, :string
      add :access_token_secret, :string
      add :handle, :string
      add :twitter_id, :bigint

      timestamps()
    end

  end
end
