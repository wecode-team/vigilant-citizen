defmodule App.Repo.Migrations.CreateDefendants do
  use Ecto.Migration

  def change do
    create table(:defendants) do
      add :name, :string

      timestamps()
    end

  end
end
