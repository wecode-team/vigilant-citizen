defmodule App.Repo.Migrations.CreateCourtAppearances do
  use Ecto.Migration

  def change do
    create table(:court_appearances) do
      add :date, :string
      add :remarks, :text
      add :defendant_id, references(:defendants, on_delete: :nothing)

      timestamps()
    end

    create index(:court_appearances, [:defendant_id])
  end
end
